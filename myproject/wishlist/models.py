from django.db import models

class WishItem(models.Model):
    title = models.CharField(max_length=100, verbose_name='Название')
    price = models.TextField(verbose_name='Цена')
    link = models.TextField(verbose_name='Ссылка')
    description = models.TextField(verbose_name='Описание')

    def __str__(self):
        return self.title
