from django.shortcuts import render
from .models import WishItem
from .serializers import WishItemSerializers
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser

class WishItemCreate(generics.CreateAPIView):
    serializer_class = WishItemSerializers
    permissions_classes = [IsAdminUser]

class WishItemList(generics.ListAPIView):
    serializer_class = WishItemSerializers
    queryset = WishItem.objects.all()

class WishItemDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = WishItemSerializers
    queryset = WishItem.objects.all()
