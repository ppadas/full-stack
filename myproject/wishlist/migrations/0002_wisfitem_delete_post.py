# Generated by Django 4.0.1 on 2022-01-13 17:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wishlist', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='WisfItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('price', models.IntegerField()),
                ('link', models.TextField(verbose_name='На ваш выбор')),
                ('description', models.TextField(verbose_name='Описание')),
            ],
        ),
        migrations.DeleteModel(
            name='Post',
        ),
    ]
