import React, {useState} from 'react';
import WishList from './WishList';
import "../style/App.css"
import snow from '../Snow.jpg';
import AddWish from './AddWish';
import Authentication from './Authentication';
import { drop, getPosts, create, update, checkLogin } from '../services/BackApi';

const data = []
let current_token = ""

function App() {
    const [authenticated, setAuthenticated] = useState(false)
    const [state, setState] = useState(data);
    const getNewId = ()=>{
        return Math.max(...state.map(elem=>elem.id), 0) + 1
    }

    const updateState = async ()=>{
        var response = await getPosts()
        setState(response)
    }

    const updateWish = async (id, title, description, price, link)=>{
        if(title === "") {
            title = "(Не указано)"
        }
        if(description === "") {
            description = "(Не указано)"
        }
        if(price === "") {
            price = "(Не указано)"
        }
        if(link === "") {
            link = "(Не указано)"
        }
        let changes = {
            "id": id,
            "title": title,
            "price": price,
            "link": link,
            "description": description
        }
        await update(id, changes, current_token)
        updateState()
    };

    const addWish = async (id, title, description, price, link)=>{
        let insert={
            "title": title,
            "price": price,
            "link": link,
            "description": description
        }
        await create(insert, current_token)
        updateState()
    };

    const deleteWish = async (id)=>{
        await drop(id, current_token)
        updateState()
    };

    const checkData = async (login, password)=> {
        let data={
            "username": login,
            "password": password
        }
        let answer = await checkLogin(data)
        if (answer.access) {
            current_token = answer.access
            setAuthenticated(true)
        } else {
            setAuthenticated(false)
            current_token = ""
        }
    }

    return (
        <div className='all_app'>
            <div className="title" onClick={updateState}>
                    <img src={snow} alt="BigCo Inc. logo" height="40" width="40" />
                    <label> My wishlist for New Year </label>
                    <img src={snow} alt="BigCo Inc. logo" height="40" width="40" />
            </div>
            <Authentication checkData={checkData} authenticated={authenticated}/>
            <WishList state={state} updateWish={updateWish} deleteWish={deleteWish}/>
            <AddWish addWish={addWish} getNewId={getNewId}/>
        </div>
    );
}

export default App;
