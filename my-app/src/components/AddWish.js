import React, {useState} from 'react';
import '../style/AddWish.css';
import '../style/WishItem.css';

function AddWish({addWish, getNewId}) {
    const [new_title, setStateTitle] = useState("(Не указано)");
    const [new_price, setStatePrice] = useState("(Не указано)");
    const [new_discr, setStateDiscr] = useState("(Не указано)");
    const [new_link, setStateLink] = useState("(Не указано)");
    const onChangeTitle = event=>{
        setStateTitle(event.target.value)
    }
    const onChangePrice = event=>{
        setStatePrice(event.target.value)
    }
    const onChangeDiscr = event=>{
        setStateDiscr(event.target.value)
    }
    const onChangeLink = event=>{
        setStateLink(event.target.value)
    }
    const Add = event=>{
        if (new_title !== "") {
            addWish(getNewId(), new_title, new_discr, new_price, new_link)
            document.getElementById("Title").value = "";
            document.getElementById("Price").value = "";
            document.getElementById("Descr").value = "";
            document.getElementById("Link").value = "";
            setStateTitle("(Не указано)");
            setStatePrice("(Не указано)");
            setStateDiscr("(Не указано)");
            setStateLink("(Не указано)");
        }
    }
    return (
        
        <div className='wish_item'>
            <div className='title'>
                <input type='text' id="Title" placeholder='Название'
                    onChange={onChangeTitle}>
                </input>
            </div>

            <label> <i>Примерная цена:</i> </label>
            <input id="Price" placeholder='Бесценно'
                onChange={onChangePrice}>
            </input>


            <label> <i>Описание:</i> </label>
            <input id="Descr" placeholder='Очень хочется'
                onChange={onChangeDiscr}>
            </input>


            <label> <i>Ссылка:</i></label>
            <input id="Link" placeholder='.-.'
                onChange={onChangeLink}>
            </input>
            <br/>
            <button onClick={Add}> Добавить</button>
        </div>
    )
}

export default AddWish;
