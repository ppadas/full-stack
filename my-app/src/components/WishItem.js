import React, {useState} from 'react';
import '../style/WishItem.css';

function WishItem(props) {
    let number = props.number
    let{id, title, description, price, link} = props.item;
    let updateWish = props.updateWish
    let deleteWish = props.deleteWish

    const [new_title, setStateTitle] = useState(title);
    const [new_price, setStatePrice] = useState(price);
    const [new_discr, setStateDiscr] = useState(description);
    const [new_link, setStateLink] = useState(link);
    const [on_change, WantChange] = useState("Default");
    const onChangeTitle = event=>{
        setStateTitle(event.target.value)
    }
    const onChangePrice = event=>{
        setStatePrice(event.target.value)
    }
    const onChangeDiscr = event=>{
        setStateDiscr(event.target.value)
    }
    const onChangeLink = event=>{
        setStateLink(event.target.value)
    }
    
    const Change = (event)=>{
        WantChange("Change")
    }
    const Add = event=>{
        updateWish(id, new_title, new_discr, new_price, new_link)
        WantChange("Default")
    }
    const Exit = event=>{
        WantChange("Default")
    }
    const Delete = event=>{
        WantChange("Default")
        deleteWish(id)
    }

    let update_state = <div className='wish_item'>
                            <div className='title'>
                                <input name='awesome' defaultValue={title} 
                                    onChange={onChangeTitle}/>
                            </div>
                            <label> <i>Примерная цена:</i> </label>
                            <input name='awesome' defaultValue={price}
                                    onChange={onChangePrice}/>
                            <br />
                            <label> <i>Ссылка:</i> </label>
                            <input name='awesome' defaultValue={link}
                                    onChange={onChangeLink}/>
                            <br />
                            <label> <i>Описание:</i> </label>
                            <input name='awesome' defaultValue={description}
                                    onChange={onChangeDiscr}/>
                            <div className='actions'>
                                <div className='in'><button onClick={Add}>Сохранить изменения</button></div>
                                <div className='in'><button onClick={Delete}>Удалить</button></div>
                                <div className='in'><button onClick={Exit}>Выйти</button></div>
                            </div>
                        </div>

    let default_state = <div className='wish_item' onDoubleClick={Change}>
                            <div className='title'>
                                <label> {number}) {title} </label>
                            </div>
                            <label> <i>Примерная цена:</i> {price}</label>
                            <br />
                            <label> <i>Ссылка:</i> {link}</label>
                            <br />
                            <label> <i>Описание:</i> {description}</label>
                        </div>

    let current_state = on_change === "Default" ? default_state : update_state

    return (
        current_state
    );
}

export default WishItem;
