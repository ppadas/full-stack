import React from 'react';
import WishItem from './WishItem';
import "../style/WishList.css"

function WishList({state, updateWish, deleteWish}) {
    
    const dict = new Map();
    var i = 0
    state.map(elem=>{dict.set(i, elem); return ++i;})
    i = 0
    const func = (elem) => {
        i++;
        return <WishItem key={i} number={i} item={elem} updateWish={updateWish} deleteWish={deleteWish} />
    }

    return (
        <div className='wish_list'>
            {state.map(func)}
        </div>
    );
}

export default WishList;
