import React, {useState} from 'react';
import "../style/WishItem.css"

function Authentication({checkData, authenticated}) {
    const [login, setStateLogin] = useState("");
    const [password, setStatePassword] = useState("");
    const onChangeLogin = (event)=>{
        setStateLogin(event.target.value)
    }
    const onChangePassword = (event)=>{
        setStatePassword(event.target.value)
    }
    const In=()=>{
        checkData(login, password)
        document.getElementById("Login").value = "";
        document.getElementById("Password").value = "";
    }

    let field = <div className='wish_item'>
            <div className='title'>
                Вход
            </div>
            <label> <b><i>Авторизуйтесь для внесения изменений</i></b> </label>
            <br/>
            <br/>
            <label> <i>Логин:</i> </label>
            <input id="Login" placeholder='Login'
                onChange={onChangeLogin}>
            </input>


            <label> <i>Пароль:</i> </label>
            <input id="Password" placeholder='....' type='password'
                onChange={onChangePassword}>
            </input>
            <br/>
            <button onClick={In}> Войти</button>
        </div>

    field = authenticated ? "" : field
    

    return (
        field
    );
}

export default Authentication;
