const baseUrl = 'http://51.250.23.198:8000/'

export const checkLogin = async (data)=>{
    return fetch(baseUrl+'auth/token/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    }).then(data=>data.json())
}

export const getPosts = async ()=>{
    return fetch(baseUrl+'api/get_all/')
    .then(data=>data.json())
}

export const drop = async (id, current_token)=>{
    return fetch(baseUrl+`api/drop/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${current_token}`,
            'Content-Type': 'application/json;charset=utf-8'
        }
    })
}

export const create = async (data, current_token)=>{
    return fetch(baseUrl+'api/create/', {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${current_token}`,
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    }).then(data=>console.log(data.json()))
}

export const update = async (id, data, current_token)=>{
    console.log(data)
    return fetch(baseUrl+`api/drop/${id}`, {
        method: 'PUT',
        headers: {
            'Authorization': `Bearer ${current_token}`,
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    })
}
